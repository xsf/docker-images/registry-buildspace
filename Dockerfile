# Base docker image for building XEPs
# Sets up directories and dependencies
# docker build . -t xmppxsf/xeps-base -f Dockerfile.base

FROM debian:10
MAINTAINER XSF Editors <editor@xmpp.org>

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        xsltproc libxml2-utils libxml2 make && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /var/www/html/registrar
